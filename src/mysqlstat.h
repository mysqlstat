/* This file is part of Mysqlstat -*- autoconf -*-
 * Copyright (C) 2016-2020 Sergey Poznyakoff
 *
 * Mysqlstat is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * Mysqlstat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Mysqlstat.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>

#include <net-snmp/net-snmp-config.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#define CACHE_TIMEOUT 10

typedef struct mysqlstat_connection *mysqlstat_connection_t;

mysqlstat_connection_t mysqlstat_connect(void);
void mysqlstat_disconnect(mysqlstat_connection_t);

uint32_t process_total_count(void);
uint32_t process_active_count(void);
uint32_t process_slave_count(void);

