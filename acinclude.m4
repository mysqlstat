# This file is part of Mysqlstat -*- autoconf -*-
# Copyright (C) 2014-2020 Sergey Poznyakoff
#
# Mysqlstat is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Mysqlstat is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Mysqlstat.  If not, see <http://www.gnu.org/licenses/>.

dnl Arguments:
dnl   $1     --    Library to look for
dnl   $2     --    Function to check in the library
dnl   $3     --    Any additional libraries that might be needed
dnl   $4     --    Action to be taken when test succeeds
dnl   $5     --    Action to be taken when test fails
dnl   $6     --    Directories where the library may reside
AC_DEFUN([MY_CHECK_LIB],
[m4_ifval([$4], , [AH_CHECK_LIB([$1])])dnl
AS_VAR_PUSHDEF([my_Lib], [my_cv_lib_$1])dnl
AC_CACHE_CHECK([for $2 in -l$1], [my_Lib],
[AS_VAR_SET([my_Lib], [no])
 my_check_lib_save_LIBS=$LIBS
 for path in "" $6
 do
   if test -n "$path"; then
     my_ldflags="-L$path -l$1 $3"
   else
     my_ldflags="-l$1 $3"
   fi
   LIBS="$my_ldflags $my_check_lib_save_LIBS"
   AC_LINK_IFELSE([AC_LANG_CALL([], [$2])],
                  [AS_VAR_SET([my_Lib], ["$my_ldflags"])
		   break])
 done		  
 LIBS=$my_check_lib_save_LIBS])
AS_IF([test "AS_VAR_GET([my_Lib])" != no],
      [m4_default([$4], [AC_DEFINE_UNQUOTED(AS_TR_CPP(HAVE_LIB$1))
  LIBS="-l$1 $LIBS"
])],
      [$5])dnl
AS_VAR_POPDEF([my_Lib])dnl
])# MY_CHECK_LIB
